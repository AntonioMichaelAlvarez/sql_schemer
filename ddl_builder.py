# coding: utf-8
__author__ = 'username'


class DDLBuilder:
    def __init__(self, file, schema_props, domains, tables):
        print("[DDLBuilder]: Построение базы данных по описателю")
        self.schema_props = schema_props
        self.domains = domains
        self.tables = tables
        # Поле script будет содержать текст на DDL
        self.script = ""

    def adapt_data_types_for_pg(self, data_type):
        if data_type == 'BLOB':
            return 'BYTEA'
        elif data_type == 'BOOLEAN':
            return 'BOOLEAN'
        elif data_type == 'BYTE':
            return 'CHAR'
        elif data_type == 'CODE':
            return 'VARCHAR(20)'
        elif data_type == 'DATE':
            return 'DATE'
        elif data_type == 'LARGEINT':
            return 'BIGINT'             # int8
        elif data_type == 'MEMO':
            return 'TEXT'
        elif data_type == 'STRING':
            return 'VARCHAR'
        elif data_type == 'TIME':
            return 'TIME'
        elif data_type == 'WORD':
            return 'SMALLINT'           # int2
        else:
            return data_type

    def crouch_domains(self):
        for domain in self.domains:
            if not domain.anonymous:
                self.script += ("CREATE DOMAIN IF NOT EXISTS \"%s\" %s%s;\n" % (
                    domain.name, self.adapt_data_types_for_pg(domain.type), ("("+str(domain.char_length)+")"
                      if str(domain.char_length) != "" else "")))

    def crouch_tables(self):
        for table in self.tables:
            self.script += ("CREATE TABLE IF NOT EXISTS \"%s\" (\n" % table.name)
            for field in table.fields:
                self.script += ("\t\"%s\"\t%s%s" % (field.name, ("\"" + field.domain.name + "\"") if not field.domain.anonymous else
                      (self.adapt_data_types_for_pg(field.domain.type) + ("("+str(field.domain.char_length)+")"
                       if str(field.domain.char_length) != "" else "")),
                    ',\n' if (table.fields.index(field)) < (len(table.fields)-1) else ''))
            self.crouch_constraints(table)
            self.script += ");\n"

    def crouch_constraints(self, table):
        # Сначала добавить первичные ключи, чтобы не нарушать целостность схемы
        # for table in self.tables:
        for constr in table.constraints:
            if constr.kind == 'PRIMARY':
                s = constr.items.replace(' ', '').replace(',', '","')
                self.script += (",\nPRIMARY KEY (\"%s\")" % s)

        # Добавить все остальные ограничения
        # for table in self.tables:
        for constr in table.constraints:
            if constr.kind == 'FOREIGN':
                self.script += ("\n,FOREIGN KEY (\"%s\") REFERENCES \"%s\"%s"
                                % (
                                    constr.items, constr.reference,
                                    " ON DELETE CASCADE" if "cascading_delete" in constr.props else ""
                                )
                                )

    def crouch_indexes(self):
        for table in self.tables:
            for idx in table.indexes:
                s = idx.field.replace(' ', '').replace(',', '","')
                self.script += ("CREATE %sINDEX \"%s_%s_idx\" ON \"%s\"(\"%s\");\n"
                                % (
                                    "UNIQUE " if "uniqueness" in idx.props else "",
                                    table.name,
                                    s.replace(' ', '').replace('"', '').replace(',', '_'),
                                    table.name,
                                    s
                                )
                                )

    def crouch_data(self):
        self.crouch_domains()
        self.crouch_tables()
        self.crouch_indexes()

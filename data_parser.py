# coding: utf-8
__author__ = 'username'
from xml.dom.minidom import parse
from op_types import Domain, Field, Index, Constraint, Table
from sql_gen import SQLGenerator


class SchemaParser:
    # Класс обработчика данных из файла
    def __init__(self, file):
        self.file = file
        self.xml_parser = parse(self.file)
        # Поля с "сырыми" данными
        self.p_domains = self.xml_parser.getElementsByTagName("domain")
        self.p_tables = self.xml_parser.getElementsByTagName("table")
        self.p_indexes = self.xml_parser.getElementsByTagName("index")
        self.p_constraints = self.xml_parser.getElementsByTagName("constraint")
        # Поля для обработанных списков
        self.schema_props = {}
        self.parsed_domains = []
        self.parsed_tables = []
        # Ошибки во время проверки?
        self.errors = False

    def collect_schema_props(self):
        # Сохранить свойства описателя
        schema_attrs = self.xml_parser.getElementsByTagName("dbd_schema")[0].attributes.keys()
        for attr in schema_attrs:
            self.schema_props[attr] = str(self.xml_parser.getElementsByTagName("dbd_schema")[0]
                                          .attributes[attr].value)

    def collect_domains(self):
        # Накопление данных о доменах
        print("[SchemaParser]: Обработка доменов")
        attr_list = ['align', 'type', 'name', 'char_length', 'width', 'description', 'props', 'scale', 'precision',
                     'length']
        for dom_elem in self.p_domains:
            domain = Domain()
            for attr in attr_list:
                domain.set_prop(attr, dom_elem.getAttribute(attr))
            self.parsed_domains.append(domain)
        print("\t\tГотово, итого записей:", len(self.parsed_domains))

    def collect_tables(self):
        # Накопление данных о таблицах
        print("[SchemaParser]: Обработка таблиц")
        attr_list = ['description', 'props', 'name']
        for dom_elem in self.p_tables:
            table = Table()
            for attr in attr_list:
                table.set_prop(attr, dom_elem.getAttribute(attr))
            self.parsed_tables.append(table)
        print("\t\tГотово, итого записей:", len(self.parsed_tables))

    def collect_fields(self):
        # Накопление данных о полях
        print("[SchemaParser]: Обработка полей")
        attr_list = ['domain', 'rname', 'props', 'name', 'description']
        for dom_elem in self.p_tables:
            tab_index = self.p_tables.index(dom_elem)
            t_prs_fields = []
            for f_elem in dom_elem.getElementsByTagName("field"):
                field = Field()
                for attr in attr_list:
                    field.set_prop(attr, f_elem.getAttribute(attr))
                if f_elem.getAttribute("domain.type") != "":
                    # Если есть этот атрибут, то поле может содержать неименованный домен
                    if field.get_prop("domain") != "":
                        print("\t\tВнимание: Поле использует именованный и"
                              " неименованный домены, выбирается именованный")
                    else:
                        anon_domain = Domain()
                        for dom_prop in ['domain.align', 'domain.type', 'domain.char_length', 'domain.width',
                                         'domain.description', 'domain.props', 'domain.scale',
                                         'domain.precision',
                                         'domain.length']:
                            anon_domain.set_prop(dom_prop.replace("domain.", ""), f_elem.getAttribute(dom_prop))
                        anon_domain.set_prop("anonymous", True)
                        anon_domain.set_prop("name", "anon_domain_%d_%d" % (
                            self.p_tables.index(dom_elem),
                            dom_elem.getElementsByTagName("field").index(f_elem)
                        ))
                        field.set_prop("domain", anon_domain.get_prop("name"))
                        self.parsed_domains.append(anon_domain)
                t_prs_fields.append(field)
            self.parsed_tables[tab_index].fields = t_prs_fields
        print("\t\tOK")

    def collect_indexes(self):
        # Накопление данных об индексах
        print("[SchemaParser]: Обработка индексов")
        attr_list = ['field', 'props', 'expression']
        for dom_elem in self.p_tables:
            tab_index = self.p_tables.index(dom_elem)
            t_prs_indexes = []
            for i_elem in dom_elem.getElementsByTagName("index"):
                index = Index()
                for attr in attr_list:
                    index.set_prop(attr, i_elem.getAttribute(attr))
                t_prs_indexes.append(index)
            self.parsed_tables[tab_index].indexes = t_prs_indexes
        print("\t\tOK")

    def collect_constraints(self):
        # Накопление данных об ограничениях
        print("[SchemaParser]: Обработка ограничений")
        attr_list = ['kind', 'items', 'reference', 'props', 'expression']
        for dom_elem in self.p_tables:
            tab_index = self.p_tables.index(dom_elem)
            t_prs_constraints = []
            for c_elem in dom_elem.getElementsByTagName("constraint"):
                constraint = Constraint()
                for attr in attr_list:
                    constraint.set_prop(attr, c_elem.getAttribute(attr))
                t_prs_constraints.append(constraint)
            self.parsed_tables[tab_index].constraints = t_prs_constraints
        print("\t\tOK")

    def collect_data(self):
        # Централизованное накопление данных из файла описателя схемы
        self.collect_schema_props()
        self.collect_domains()
        self.collect_tables()
        self.collect_fields()
        self.collect_indexes()
        self.collect_constraints()

    def verify_domains(self):
        # Проверка на корректность данных доменов
        err = False
        print("[SchemaParser]: Проверка доменов")
        name_set = set()
        for domain in self.parsed_domains:
            if "" == domain.get_prop("name"):
                print("\t\tОшибка: имя домена не может быть пустым")
                err = True
                continue
            if domain.get_prop("name") in name_set:
                print("\t\tОшибка: обнаружен дубликат домена", domain.get_prop("name"))
                err = True
            else:
                name_set.add(domain.get_prop("name"))
        if not err:
            print("\t\tOK")
        self.errors |= err

    def verify_tables(self):
        # Проверка на корректность данных таблиц
        err = False
        print("[SchemaParser]: Проверка таблиц")
        name_set = set()
        for table in self.parsed_tables:
            tab_name = table.get_prop("name")
            if "" == tab_name:
                print("\t\tОшибка: имя таблицы не может быть пустым")
                err = True
                continue
            if tab_name in name_set:
                print("\t\tОшибка: обнаружен дубликат таблицы", tab_name)
                err = True
            else:
                name_set.add(table.get_prop("name"))
        if not err:
            print("\t\tOK")
        self.errors |= err

    def check_domain(self, domain):
        # Проверка существования заданного домена domain
        for __i in self.parsed_domains:
            if __i.get_prop("name") == domain:
                return True
        return False

    def find_table(self, name):
        # Проверка существования заданной таблицы name
        for table in self.parsed_tables:
            if table.get_prop("name") == name:
                return table
        return None

    def verify_fields(self):
        # Проверка на корректность данных полей таблиц
        err = False
        print("[SchemaParser]: Проверка полей")
        for table in self.parsed_tables:
            tab_name = table.get_prop("name")
            name_set = set()
            for field in table.fields:
                field_name = field.get_prop("name")
                if "" == field.get_prop("name"):
                    print("\t\tОшибка: пустое имя поля в таблице %s" % tab_name)
                    err = True
                if field_name in name_set:
                    print("\t\tОшибка: обнаружен дубликат поля %s в таблице %s" % (field_name, tab_name))
                    err = True
                else:
                    name_set.add(field_name)
                if not self.check_domain(field.get_prop("domain")):
                    print("\t\tОшибка: неизвестный домен %s у поля %s" % (
                        field.get_prop("domain"), field_name))
                    err = True
        if not err:
            print("\t\tOK")
        self.errors |= err

    def verify_indexes(self):
        # Проверка на корректность данных индексов таблиц
        err = False
        print("[SchemaParser]: Проверка индексов")
        for table in self.parsed_tables:
            for index in table.indexes:
                fields = table.find_field(index.get_prop("field"))
                # Для всех полей, по которым строится индекс
                for f in fields.replace(" ", "").split(","):
                    if table.find_field(f) is None:
                        print("\t\tОшибка: индекс по отсутствующему полю %s в таблице %s"
                              % (f, table.get_prop("name")))
                        err = True
        if not err:
            print("\t\tOK")
        self.errors |= err

    def verify_constraints(self):
        # Проверка на корректность данных ограничений таблиц
        err = False
        print("[SchemaParser]: Проверка ограничений")
        table_set = set()
        for table in self.parsed_tables:
            table_set.add(table.get_prop("name"))
        for table in self.parsed_tables:
            tab_name = table.get_prop("name")
            field_set = set()
            for field in table.fields:
                field_set.add(field.get_prop("name"))
            for constraint in table.constraints:
                # Проверяем, существует ли поле в данной таблице
                # if constraint.get_prop("items") not in field_set:
                fields = table.find_field(constraint.get_prop("items"))
                test_failed = False
                # Для всех полей, на которые ссылается ограничение
                for f in fields.replace(" ", "").split(","):
                    test_failed |= (table.find_field(f) is None)
                # if table.find_field(constraint.get_prop("items")) is None:
                if test_failed:
                    print("\t\tОшибка: ограничение %s ссылается на отсутствующее поле %s в таблице %s"
                          % ("FK" if constraint.get_prop("kind") == "FOREIGN" else "PK",
                             constraint.get_prop("items"),
                             tab_name))
                    err = True
                else:
                    # Проверка для внешнего ключа
                    if constraint.get_prop("kind") == "FOREIGN":
                        # Проверить, существует ли таблица, на которую ссылается ограничение
                        # if constraint.get_prop("reference") not in table_set:
                        if self.find_table(constraint.get_prop("reference")) is None:
                            print("\t\tОшибка: внешний ключ ссылается на отсутствующую таблицу %s в таблице %s"
                                  % (constraint.get_prop("reference"), tab_name))
                            err = True
                        else:
                            # Проверить существование первичного ключа во внешней таблице
                            table_ref = self.find_table(constraint.get_prop("reference"))
                            field_ext = None
                            for constraint_ext in table_ref.constraints:
                                if constraint_ext.get_prop("kind") == "PRIMARY":
                                    field_ext = table_ref.find_field(constraint_ext.get_prop("items"))
                                    break
                            if field_ext is None:
                                print("\t\tОшибка: обращение FK к отсутствующему внешнему ключу таблицы %s в таблице %s"
                                      % (constraint.get_prop("reference"),
                                         table.get_prop("name")))
                                err = True
                            else:
                                # Проверить домены
                                if table.find_field(constraint.get_prop("items")).get_prop("domain") != \
                                        field_ext.get_prop("domain"):
                                    print("\t\tОшибка: несовпадение доменов у полей '%s'.'%s' и '%s'.'%s'" %
                                          (
                                              table.get_prop("name"),
                                              constraint.get_prop("items"),
                                              constraint.get_prop("reference"),
                                              field_ext.get_prop("name")
                                          ))
                                    err = True
        if not err:
            print("\t\tOK")
        self.errors |= err

    def verify_elements(self):
        # Централизованная проверка накопленных данных
        self.verify_domains()
        self.verify_tables()
        self.verify_fields()
        self.verify_indexes()
        self.verify_constraints()

    def init_generator(self):
        # Инициализация заполнителя базы
        if self.errors:
            print("[SchemaParser]: Невозможно сгенерировать SQL из-за наличия ошибок. Проверьте лог программы")
            exit(1)
        else:
            print("[SchemaParser]: Ошибок нет, начинается генерация SQL")
            return SQLGenerator(self.file,
                                self.schema_props,
                                self.parsed_domains,
                                self.parsed_tables)

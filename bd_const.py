# coding: utf-8
__author__ = 'username'

SQL_BD_PRE_INIT = """
pragma foreign_keys = on;
begin transaction;
"""

SQL_BD_PURGE = """
DROP TABLE IF EXISTS dbd$schema_props;
DROP TABLE IF EXISTS dbd$domains;
DROP TABLE IF EXISTS dbd$tables;
DROP TABLE IF EXISTS dbd$fields;
DROP TABLE IF EXISTS dbd$constraints;
DROP TABLE IF EXISTS dbd$constraint_details;
DROP TABLE IF EXISTS dbd$indexes;
DROP TABLE IF EXISTS dbd$index_details;
DROP TABLE IF EXISTS dbd$data_types;
"""

SQL_BD_SCHEMA_PROPS_INIT = """
--
-- Атрибуты схемы
--
create table dbd$schema_props (
id integer primary key autoincrement default(null),
prop_name varchar unique default(null), -- Имя свойства
prop_value varchar default(null)        -- Значение свойства
);
"""

SQL_BD_DOMAINS_INIT = """
--
-- Домены
--
create table dbd$domains (
    id  integer primary key autoincrement default(null),
    name varchar unique default(null),  -- имя домена
    description varchar default(null),  -- описание
    data_type integer not null,      -- идентификатор типа (dbd$data_types)
    length integer default(null),       -- длина
    char_length integer default(null),  -- длина в символах
    precision integer default(null),    -- точность
    scale integer default(null),        -- количество знаков после запятой
    width integer default(null),        -- ширина визуализации в символах
    align char default(null),           -- признак выравнивания
    show_null boolean default(null),    -- нужно показывать нулевое значение?
    show_lead_nulls boolean default(null),      -- следует ли показывать лидирующие нули?
    thousands_separator boolean default(null),  -- нужен ли разделитель тысяч?
    summable boolean default(null),             -- признак того, что поле является суммируемым
    case_sensitive boolean default(null),       -- признак необходимости регистронезависимого поиска для поля
    anonymous boolean default(null)   -- анонимный домен
);
"""

SQL_BD_TABLES_INIT = """
--
-- Каталог таблиц
--
create table dbd$tables (
    id integer primary key autoincrement default(null),
    name varchar unique,                  -- имя таблицы
    description varchar default(null),    -- описание
    can_add boolean default(null),        -- разрешено ли добавление в таблицу
    can_edit boolean default(null),       -- разрешено ли редактирование  таблице?
    can_delete boolean default(null)     -- разрешено ли удаление в таблице
);
"""

SQL_BD_FIELDS_INIT = """
--
-- Поля таблиц
--
create table dbd$fields (
    id integer primary key autoincrement default(null),
    table_id integer not null,             -- идентификатор таблицы, которой принадлежит поле
    position integer not null,             -- номер поля в таблице (для упорядочивания полей)
    name varchar not null,                 -- латинское имя поля (будет использовано в схеме Oracle)
    rname varchar not null,                -- русское имя поля для отображения пользователю в интерактивных режимах
    description varchar default(null),     -- описание
    domain_id integer not null,            -- идентификатор типа поля (dbd$domains)
    can_input boolean default(null),       -- разрешено ли пользователю вводить значение в поле?
    can_edit boolean default(null),        -- разрешено ли пользователю изменять значение в поле?
    show_in_grid boolean default(null),    -- следует ли отображать значение поля в браузере таблицы?
    show_in_details boolean default(null), -- следует ли отображать значение поля в полной информации о записи таблицы?
    is_mean boolean default(null),         -- является ли поле элементом описания записи таблицы?
    autocalculated boolean default(null),  -- признак того, что значение в поле вычисляется программным кодом
    required boolean default(null)        -- признак того, что поле дорлжно быть заполнено
);
"""

SQL_BD_CONSTRAINTS_INIT = """
--
-- Ограничения
--
create table dbd$constraints (
    id integer primary key autoincrement default (null),
    table_id integer not null,                           -- идентификатор таблицы (dbd$tables)
    constraint_type char default(null),                  -- вид ограничения
    reference integer default(null),        -- идентификатор таблицы (dbd$tables), на которую ссылается внешний ключ
    has_value_edit boolean default(null),   -- признак наличия поля ввода ключа
    cascading_delete boolean default(null), -- каскадное удаление ссылок (ID -> NULL)
    full_cascading_delete boolean default(null), -- каскадное удаление элементов (DELETE)
    expression varchar default(null)       -- выражение для контрольного ограничения
);

create table dbd$constraint_details (
    id integer primary key autoincrement default(null),
    constraint_id integer not null,          -- идентификатор ограничения
    position integer not null,               -- номер элемента ограничения
    field_id integer not null default(null)  -- идентификатор поля (dbd$fields) в таблице, для которой определено ограничение
);
"""

SQL_BD_INDEXES_INIT = """
--
-- Индексы
--
create table dbd$indexes (
    id integer primary key autoincrement default(null),
    table_id integer not null,                         -- идентификатор таблицы (dbd$tables)
    global boolean default(null),                       -- показывает тип индекса: локальный или глобальный
--  kind char default(null)                            -- вид индекса (простой/уникальный/полнотекстовый) (NULL/U/F)
    is_unique boolean default(null),                   -- уникальный
    is_fulltext boolean default(null)                  -- полнотекстовый

);

--
-- Детали индексов
--
create table dbd$index_details (
    id integer primary key autoincrement default(null),
    index_id integer not null,                          -- идентификатор индекса (dbd$indexes)
    position integer not null,                          -- порядковый номер элемента индекса
    field_id integer default(null),                     -- идентификатор поля (dbd$fields), участвующего в индексе
    expression varchar default(null),                   -- выражение для индекса
    descend boolean default(null)                       -- направление сортировки
);
"""

SQL_BD_SCHEMA_INDEXES = """
-- TODO: Implement indicies on every schema table
"""

SQL_BD_DATA_TYPES_INIT = """
--
-- Типы данных
--
create table dbd$data_types (
    id integer primary key autoincrement, -- идентификатор типа
    type_id varchar unique not null       -- имя типа
);

insert into dbd$data_types(type_id) values ('STRING');
insert into dbd$data_types(type_id) values ('SMALLINT'); -- 2B
insert into dbd$data_types(type_id) values ('INTEGER'); -- 4B
insert into dbd$data_types(type_id) values ('WORD');
insert into dbd$data_types(type_id) values ('BOOLEAN');
insert into dbd$data_types(type_id) values ('FLOAT'); -- DOUBLE 8B
insert into dbd$data_types(type_id) values ('CURRENCY'); -- FLOAT
insert into dbd$data_types(type_id) values ('BCD'); -- BCD
insert into dbd$data_types(type_id) values ('FMTBCD'); -- BCD
insert into dbd$data_types(type_id) values ('DATE');
insert into dbd$data_types(type_id) values ('TIME');
insert into dbd$data_types(type_id) values ('DATETIME');
insert into dbd$data_types(type_id) values ('TIMESTAMP');
insert into dbd$data_types(type_id) values ('BYTES'); -- Массив фиксированной длины
insert into dbd$data_types(type_id) values ('VARBYTES'); -- Нефикс
insert into dbd$data_types(type_id) values ('BLOB');
insert into dbd$data_types(type_id) values ('MEMO'); -- Текст
insert into dbd$data_types(type_id) values ('GRAPHIC'); -- Изображение - BLOB
insert into dbd$data_types(type_id) values ('FMTMEMO'); -- CLOB Oracle
insert into dbd$data_types(type_id) values ('FIXEDCHAR');
insert into dbd$data_types(type_id) values ('WIDESTRING'); -- Unicode-string
insert into dbd$data_types(type_id) values ('LARGEINT'); -- Int64
insert into dbd$data_types(type_id) values ('COMP'); -- Int64
insert into dbd$data_types(type_id) values ('ARRAY'); -- Array of bytes
insert into dbd$data_types(type_id) values ('FIXEDWIDECHAR');
insert into dbd$data_types(type_id) values ('WIDEMEMO');
insert into dbd$data_types(type_id) values ('BYTE');
insert into dbd$data_types(type_id) values ('CODE');
insert into dbd$data_types(type_id) values ('RECORDID');
insert into dbd$data_types(type_id) values ('SET');
insert into dbd$data_types(type_id) values ('PERIOD');
"""

COMMIT = """
commit;
"""

SQL_BD_INIT = SQL_BD_PRE_INIT + SQL_BD_PURGE + SQL_BD_SCHEMA_PROPS_INIT + SQL_BD_DOMAINS_INIT + \
    SQL_BD_TABLES_INIT + SQL_BD_FIELDS_INIT + \
    SQL_BD_INDEXES_INIT + SQL_BD_CONSTRAINTS_INIT + SQL_BD_SCHEMA_INDEXES + SQL_BD_DATA_TYPES_INIT + COMMIT

DML_ADD_SCHEMA_PROPS = """
INSERT INTO dbd$schema_props
(prop_name, prop_value) VALUES (?, ?)
"""

DML_ADD_DOMAIN = """
INSERT INTO dbd$domains
(name,
description,
data_type,
length,
char_length,
precision,
scale,
width,
align,
show_null,
show_lead_nulls,
thousands_separator,
summable,
case_sensitive,
anonymous) VALUES (
?, ?, (SELECT id FROM dbd$data_types WHERE type_id = ?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
)
"""


DML_ADD_TABLE = """
INSERT INTO dbd$tables
(name,
description,
can_add,
can_edit,
can_delete) VALUES (
?, ?, ?, ?, ?
)
"""

SQL_CREATE_TEMP_FIELDS = """
DROP TABLE IF EXISTS temp_dbd$fields;
create temp table temp_dbd$fields (
    id integer primary key autoincrement default(null),
    t_id varchar not null,
    position integer not null,
    name varchar not null,
    rname varchar not null,
    description varchar default(null),
    d_id varchar not null,
    can_input boolean default(null),
    can_edit boolean default(null),
    show_in_grid boolean default(null),
    show_in_details boolean default(null),
    is_mean boolean default(null),
    autocalculated boolean default(null),
    required boolean default(null)
);
"""

DML_ADD_FIELD = """
INSERT INTO temp_dbd$fields
(t_id,
position,
name,
rname,
description,
d_id,
can_input,
can_edit,
show_in_grid,
show_in_details,
is_mean,
autocalculated,
required) VALUES (
?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
"""

DML_JOIN_AND_SAVE_FIELDS = """
INSERT INTO dbd$fields
(table_id,
position,
name,
rname,
description,
domain_id,
can_input,
can_edit,
show_in_grid,
show_in_details,
is_mean,
autocalculated,
required)
SELECT
dbd$tables.id table_id,
temp_dbd$fields.position,
temp_dbd$fields.name,
temp_dbd$fields.rname,
temp_dbd$fields.description,
dbd$domains.id domain_id,
temp_dbd$fields.can_input,
temp_dbd$fields.can_edit,
temp_dbd$fields.show_in_grid,
temp_dbd$fields.show_in_details,
temp_dbd$fields.is_mean,
temp_dbd$fields.autocalculated,
temp_dbd$fields.required
FROM temp_dbd$fields
LEFT JOIN dbd$domains
ON temp_dbd$fields.d_id = dbd$domains.name
LEFT JOIN dbd$tables
ON temp_dbd$fields.t_id = dbd$tables.name
"""

SQL_CREATE_TEMP_INDEXES = """
DROP TABLE IF EXISTS temp_dbd$indexes;
CREATE TEMP TABLE temp_dbd$indexes (
    id integer primary key autoincrement default(null),
    t_name varchar not null,
    name varchar default(null),
    global boolean default(null),
    is_unique boolean default(null),
    is_fulltext boolean default(null)
);
"""

SQL_CREATE_TEMP_INDEXES_DETAILS = """
DROP TABLE IF EXISTS temp_dbd$index_details;
CREATE TEMP TABLE temp_dbd$index_details (
id integer primary key autoincrement default(null),
index_name varchar not null,
position integer not null,
field_name varchar default(null),
expression varchar default(null),
descend boolean default(null),
tab_name varchar not null
)
"""

DML_ADD_INDEX = """
INSERT INTO temp_dbd$indexes (
t_name,
name,
global,
is_unique,
is_fulltext
) VALUES (?, ?, ?, ?, ?);
"""

DML_ADD_INDEX_DETAIL = """
INSERT INTO temp_dbd$index_details (
index_name,
position,
field_name,
expression,
descend,
tab_name
) VALUES (?, ?, ?, ?, ?, ?)
"""

DML_JOIN_AND_SAVE_INDEXES = """
INSERT INTO dbd$indexes
SELECT
temp_dbd$indexes.id,
dbd$tables.id table_id,
temp_dbd$indexes.global,
temp_dbd$indexes.is_unique,
temp_dbd$indexes.is_fulltext
FROM temp_dbd$indexes
LEFT JOIN dbd$tables
ON temp_dbd$indexes.t_name = dbd$tables.name;

INSERT INTO dbd$index_details
SELECT
temp_dbd$index_details.id,
temp_dbd$indexes.id,
temp_dbd$index_details.position,
dbd$fields.id,
temp_dbd$index_details.expression,
temp_dbd$index_details.descend
FROM temp_dbd$index_details
LEFT JOIN temp_dbd$indexes
ON temp_dbd$index_details.index_name = temp_dbd$indexes.name
LEFT JOIN dbd$tables
ON temp_dbd$index_details.tab_name = dbd$tables.name
LEFT JOIN dbd$fields
ON (temp_dbd$index_details.field_name = dbd$fields.name) AND (dbd$fields.table_id = dbd$tables.id);
"""

SQL_CREATE_TEMP_CONSTRAINTS = """
DROP TABLE IF EXISTS temp_dbd$constraints;
CREATE TEMP TABLE temp_dbd$constraints (
    id integer primary key autoincrement default (null),
    table_name varchar not null,
    name varchar default(null),
    constraint_type char default(null),
    reference_name varchar default(null),
    has_value_edit boolean default(null),
    cascading_delete boolean default(null),
    full_cascading_delete boolean default(null),
    expression varchar default(null)
);
"""

SQL_CREATE_TEMP_CONSTRAINT_DETAILS = """
DROP TABLE IF EXISTS temp_dbd$constraint_details;
CREATE TEMP TABLE temp_dbd$constraint_details (
    id integer primary key autoincrement default(null),
    constraint_name varchar not null,
    position integer not null,
    field_name varchar not null default(null),
    tab_name varchar not null
);
"""

DML_ADD_CONSTRAINT = """
INSERT INTO temp_dbd$constraints (
table_name,
name,
constraint_type,
reference_name,
has_value_edit,
cascading_delete,
full_cascading_delete,
expression
) VALUES (?, ?, ?, ?, ?, ?, ?, ?);
"""

DML_ADD_CONSTRAINT_DETAIL = """
INSERT INTO temp_dbd$constraint_details (
constraint_name,
position,
field_name,
tab_name) VALUES (?, ?, ?, ?);
"""

DML_JOIN_AND_SAVE_CONSTRAINTS = """
INSERT INTO dbd$constraints
SELECT
const_id,
table_id,
constraint_type,
dbd$tables.id reference_id,
has_value_edit,
cascading_delete,
full_cascading_delete,
expression
FROM
(SELECT
temp_dbd$constraints.id const_id,
dbd$tables.id table_id,
temp_dbd$constraints.constraint_type,
temp_dbd$constraints.reference_name,
temp_dbd$constraints.has_value_edit,
temp_dbd$constraints.cascading_delete,
temp_dbd$constraints.full_cascading_delete,
temp_dbd$constraints.expression
FROM temp_dbd$constraints
LEFT JOIN dbd$tables
ON temp_dbd$constraints.table_name = dbd$tables.name)
LEFT JOIN dbd$tables
ON reference_name = dbd$tables.name;

INSERT INTO dbd$constraint_details
SELECT
temp_dbd$constraint_details.id,
temp_dbd$constraints.id const_id,
temp_dbd$constraint_details.position,
dbd$fields.id field_id
FROM temp_dbd$constraint_details
LEFT JOIN temp_dbd$constraints
ON temp_dbd$constraint_details.constraint_name = temp_dbd$constraints.name
LEFT JOIN dbd$tables
ON temp_dbd$constraint_details.tab_name = dbd$tables.name
LEFT JOIN dbd$fields
ON (temp_dbd$constraint_details.field_name = dbd$fields.name) AND (dbd$fields.table_id = dbd$tables.id)
"""

SQL_GET_SCHEMA_PROPS = """
SELECT
dbd$schema_props.prop_name,
dbd$schema_props.prop_value
FROM dbd$schema_props
"""

SQL_GET_DOMAINS = """
SELECT
dbd$domains.name,
dbd$domains.description,
dbd$data_types.type_id,
dbd$domains.length,
dbd$domains.char_length,
dbd$domains.precision,
dbd$domains.scale,
dbd$domains.width,
dbd$domains.align,
dbd$domains.anonymous,
dbd$domains.show_null,
dbd$domains.show_lead_nulls,
dbd$domains.thousands_separator,
dbd$domains.summable,
dbd$domains.case_sensitive
FROM dbd$domains
LEFT JOIN dbd$data_types
ON dbd$domains.data_type = dbd$data_types.id
"""

SQL_GET_TABLES = """
SELECT
dbd$tables.name,
dbd$tables.description,
dbd$tables.can_add,
dbd$tables.can_edit,
dbd$tables.can_delete
FROM dbd$tables
"""

SQL_GET_FIELDS = """
SELECT
dbd$fields.table_id,
dbd$fields.position,
dbd$fields.name,
dbd$fields.rname,
dbd$fields.domain_id,
dbd$fields.description,
dbd$fields.can_input,
dbd$fields.can_edit,
dbd$fields.show_in_grid,
dbd$fields.show_in_details,
dbd$fields.is_mean,
dbd$fields.autocalculated,
dbd$fields.required
FROM dbd$fields
ORDER BY table_id, position
"""

SQL_GET_INDICIES = """
SELECT
dbd$indexes.table_id,
dbd$index_details.position,
dbd$fields.name,
dbd$index_details.expression,
dbd$indexes.is_unique,
dbd$indexes.is_fulltext,
dbd$index_details.descend,
dbd$indexes.global
FROM dbd$index_details
LEFT JOIN dbd$indexes
ON dbd$index_details.index_id = dbd$indexes.id
LEFT JOIN dbd$fields
ON dbd$index_details.field_id = dbd$fields.id
GROUP BY dbd$indexes.table_id, dbd$index_details.position
"""

SQL_GET_CONSTRAINTS = """
SELECT
dbd$constraints.table_id,
dbd$constraint_details.position,
dbd$constraints.constraint_type,
dbd$fields.name,
dbd$tables.name,
dbd$constraints.expression,
dbd$constraints.has_value_edit,
dbd$constraints.cascading_delete,
dbd$constraints.full_cascading_delete
FROM dbd$constraint_details
LEFT JOIN dbd$constraints
ON dbd$constraint_details.constraint_id=dbd$constraints.id
LEFT JOIN dbd$fields
ON dbd$constraint_details.field_id = dbd$fields.id
LEFT JOIN dbd$tables
ON dbd$constraints.reference = dbd$tables.id
GROUP BY dbd$constraints.table_id, dbd$constraint_details.position
"""

# Код получения списка доменов
SQL_GET_DB_DOMAINS = """
SELECT
domain_name,
udt_name,
character_maximum_length
FROM
information_schema.domains
WHERE
domain_schema = 'public';
"""

# Код получения структуры таблиц БД
SQL_GET_DB_TABLES = """
SELECT
table_name,
ordinal_position,
column_name,
is_nullable,
domain_name,
udt_name,
character_maximum_length
FROM
information_schema.columns
WHERE
table_schema='public'
ORDER BY table_name, ordinal_position;
"""

# Код получения списка индексов БД
SQL_GET_DB_INDEXES = """
select
tablename,
trim('()' from replace(substring(indexdef from '\(.*\)$'), '"', '')) "fieldname"
from pg_indexes
where schemaname='public'
ORDER BY tablename, fieldname;
"""

# Код получения ограничений
SQL_GET_DB_CONSTRAINTS = """
--NEW:
SELECT
tn,
ct,
cn,
ft,
string_agg(fc,',')
FROM
(SELECT
tc.table_name as tn,
tc.constraint_type as ct,
string_agg(kcu.column_name,',') as cn,
ccu.table_name as ft,
ccu.column_name as fc
FROM information_schema.table_constraints AS tc
JOIN information_schema.key_column_usage AS kcu
ON tc.constraint_name = kcu.constraint_name
JOIN information_schema.constraint_column_usage AS ccu
ON ccu.constraint_name = tc.constraint_name
WHERE constraint_type in ('FOREIGN KEY','PRIMARY KEY')
GROUP BY tn, ct, ft, fc) AS subres
GROUP BY tn, ct, cn, ft
ORDER BY tn, ct;
"""

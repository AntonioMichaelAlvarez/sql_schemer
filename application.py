# coding: utf-8
# Точка входа в приложение
import time
import getpass
import argparse
from data_parser import SchemaParser
from sql_parse import DBParser
from ddl_builder import DDLBuilder
from xml_gen import XMLGen
from db_schema_extractor import SchemaExtractor
from ordered_args import OrderedArgs


ap = argparse.ArgumentParser(description='Инструмент для создания и конвертирования '
                                         'представлений базы данных (PostgreSQL-only version)')
ap.add_argument('-r', '--to-rel', metavar='xml_file', type=str, nargs='?', action=OrderedArgs,
                help='Восстановление реляционного описания БД из XML-описателя из запись его в SQLite-базу')
ap.add_argument('-x', '--to-xml', metavar='sqlite_file', type=str, nargs='?', action=OrderedArgs,
                help='Восстановление XML-описателя из SQLite-базы')
ap.add_argument('-d', '--to-ddl', metavar='ddl_source', type=str, nargs='?', action=OrderedArgs,
                help='Генерация DDL из реляционного представления в SQLite-базе (для PostgreSQL)')
ap.add_argument('-g', '--gen-descriptor', metavar='db_target', type=str, nargs='?', action=OrderedArgs,
                help='Генерация XML-описателя БД (PostgreSQL)')
ap.add_argument('-m', '--migrate', metavar='db_source', type=str, nargs='?', action=OrderedArgs,
                help='Сохранение данных из указанной базы в новую базу (PostgreSQL).'
                     'Следует указывать пользователя, имеющего возможность удалять и создавать базы данных')


args_list = ap.parse_args().ordered_args
print(args_list)
for argpair in args_list:
    if argpair[0] == "to_rel":
        # Обработать XML-описатель и поместить реляционное представление базы в файл sqlite
        data_parser = SchemaParser(argpair[1])
        st_time = time.time()
        data_parser.collect_data()
        print("[Application]: Накопление данных: %d ms" % ((time.time() - st_time)*1000))
        st_time = time.time()
        data_parser.verify_elements()
        print("[Application]: Проверка данных: %d ms" % ((time.time() - st_time)*1000))
        st_time = time.time()
        db_data = data_parser.init_generator()
        db_data.write_sql()
        print("[Application]: Запись в базу: %d ms" % ((time.time() - st_time)*1000))
    elif argpair[0] == "to_xml":
        # Восстановить описатель XML из файла sqlite
        st_time = time.time()
        db_data = DBParser(argpair[1])
        schema_data = db_data.fetch_data()
        print("[Application]: Извлечение из базы: %d ms" % ((time.time() - st_time)*1000))
        xml_gen = XMLGen(schema_data[0], schema_data[1], schema_data[2], schema_data[3])
        xml_gen.print_data()
    elif argpair[0] == "to_ddl":
        # Сгенерировать DDL из реляционного представляения (sqlite)
        st_time = time.time()
        db_data = DBParser(argpair[1])
        schema_data = db_data.fetch_data()
        ddl_builder = DDLBuilder(schema_data[0], schema_data[1], schema_data[2], schema_data[3])
        ddl_builder.crouch_data()

    elif argpair[0] == "migrate":
        # Подключиться к PGSQL и создать копию базы с переносом всех данных
        db_host = input("Хост БД: ")  # localhost
        db_source = input("Исходная база: ")  # northwind
        db_target = input("Целевая база: ")  # new_northwind
        db_user = input("Пользователь: ")  # postgres
        db_pass = getpass.getpass("Пароль: ")  # postgres
        se = SchemaExtractor(db_host, db_user, db_pass, db_source, db_target, mode="migrating")
        se.copy_data()
    elif argpair[0] == "gen_descriptor":
        # Подключиться к PGSQL и сгенерировать XML-описатель указанной базы
        db_host = input("Хост БД: ")  # localhost
        db_source = input("Исходная база: ")  # northwind
        db_user = input("Пользователь: ")  # postgres
        db_pass = getpass.getpass("Пароль: ")  # postgres
        se = SchemaExtractor(db_host, db_user, db_pass, db_source, "", mode="describe")
        se.gen_xml()
exit(0)

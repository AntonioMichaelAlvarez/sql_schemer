# coding: utf-8
__author__ = 'username'
import sqlite3, bd_const
from op_types import *
from bd_const import *


class DBParser:
    # Класс обработчика данных из базы
    def __init__(self, file):
        print("[DBParser]: Подключение к базе %s" % file)
        self.file = file
        self.conn = sqlite3.connect(file)
        self.cur = self.conn.cursor()
        self.schema_props, self.domains, self.tables = {}, [], []
        # Список свойст элементов схемы
        self.prop_list_for = dict(
            domain = ["show_null", "show_lead_nulls", "thousands_separator", "summable", "case_sensitive"],
            table = ["add", "edit", "delete"],
            field = ["input", "edit", "show_in_grid", "show_in_details", "is_mean", "autocalculated", "required"],
            index = ["uniqueness", "fulltext", "descend", "global"],
            constraint = ["has_value_edit", "cascading_delete", "full_cascading_delete"]
        )


    def props_to_str(self, prop_list, element, shift):
        # Вспомогательная функция, превращающая список свойств в строку
        prop_str = ""
        for i in range(shift, shift + len(prop_list)):
            if element[i] == "TRUE":
                prop_str += prop_list[i - shift] + ','
        return prop_str.rstrip(',')

    def fetch_schema_props(self):
        # Извлечение свойств схемы
        print("[DBParser]: Извлечение свойств схемы")
        for schema_prop in self.cur.execute(SQL_GET_SCHEMA_PROPS):
            self.schema_props[str(schema_prop[0])] = str(schema_prop[1])

    def fetch_domains(self):
        # Извлечение данных доменов
        print("[DBParser]: Извлечение доменов")
        for domain in self.cur.execute(SQL_GET_DOMAINS):
            self.domains.append(self.build_domain(domain))

    def fetch_tables(self):
        # Извлечение данных таблиц
        print("[DBParser]: Извлечение таблиц")
        for table in self.cur.execute(SQL_GET_TABLES):
            self.tables.append(self.build_table(table))

    def fetch_fields(self):
        # Извлечение данных полей таблиц
        print("[DBParser]: Извлечение полей таблиц")
        for field in self.cur.execute(SQL_GET_FIELDS):
            self.process_field_data(field)

    def fetch_indicies(self):
        # Извлечение данных индексов таблиц
        print("[DBParser]: Извлечение индексов таблиц")
        for index in self.cur.execute(SQL_GET_INDICIES):
            self.process_index_data(index)

    def fetch_constraints(self):
        # Извлечение данных ограничений таблиц
        print("[DBParser]: Извлечение ограничений таблиц")
        for constraint in self.cur.execute(SQL_GET_CONSTRAINTS):
            self.process_constraint_data(constraint)

    def fetch_data(self):
        # Централизованное извлечение всех данных из БД
        self.fetch_schema_props()
        self.fetch_domains()
        self.fetch_tables()
        self.fetch_fields()
        self.fetch_indicies()
        self.fetch_constraints()
        return self.file, self.schema_props, self.domains, self.tables

    def test_data(self, data):
        # Вспомогательная функция для проверки данных на пустоту
        if data == "NULL":
            return ""
        else:
            return data

    def build_domain(self, domain):
        # Обработка данных из БД о домене
        domain_elem = Domain()
        domain_elem.set_prop("name", domain[0])
        domain_elem.set_prop("description", self.test_data(domain[1]))
        domain_elem.set_prop("type", self.test_data(domain[2]))
        domain_elem.set_prop("length", self.test_data(domain[3]))
        domain_elem.set_prop("char_length", self.test_data(domain[4]))
        domain_elem.set_prop("precision", self.test_data(domain[5]))
        domain_elem.set_prop("scale", self.test_data(domain[6]))
        domain_elem.set_prop("width", self.test_data(domain[7]))
        domain_elem.set_prop("align", self.test_data(domain[8]))
        domain_elem.set_prop("anonymous", self.test_data(domain[9]))
        domain_elem.set_prop("props", self.test_data(self.props_to_str(self.prop_list_for['domain'], domain, 10)))
        return domain_elem

    def build_table(self, table):
        # Обработка данных из БД о таблице
        table_elem = Table()
        table_elem.set_prop("name", table[0])
        table_elem.set_prop("description", self.test_data(table[1]))
        table_elem.set_prop("props", self.test_data(self.props_to_str(self.prop_list_for['table'], table, 2)))
        return table_elem

    def process_field_data(self, field):
        # Обработка данных из БД о поле
        field_elem = Field()
        field_elem.set_prop("name", field[2])
        field_elem.set_prop("rname", self.test_data(field[3]))
        field_elem.set_prop("domain", self.domains[field[4]-1])
        field_elem.set_prop("description", self.test_data(field[5]))
        field_elem.set_prop("props", self.test_data(self.props_to_str(self.prop_list_for['field'], field, 6)))
        self.tables[field[0]-1].fields.append(field_elem)

    def process_index_data(self, index):
        # Обработка данных из БД об индексе
        ind_elem = Index()
        ind_elem.set_prop("field", index[2])
        ind_elem.set_prop("expression", self.test_data(index[3]))
        ind_elem.set_prop("props", self.test_data(self.props_to_str(self.prop_list_for['index'], index, 4)))
        self.tables[index[0]-1].indexes.append(ind_elem)

    def process_constraint_data(self, constraint):
        # Обработка данных из БД об ограничении
        const_elem = Constraint()
        const_elem.set_prop("kind", "PRIMARY" if constraint[2] is "P" else "FOREIGN")
        const_elem.set_prop("items", constraint[3])
        const_elem.set_prop("reference", "" if const_elem.get_prop("kind") is "PRIMARY" else self.test_data(constraint[4]))
        const_elem.set_prop("expression", self.test_data(constraint[5]))
        const_elem.set_prop("props", self.test_data(self.props_to_str(self.prop_list_for['constraint'], constraint, 6)))
        self.tables[constraint[0]-1].constraints.append(const_elem)


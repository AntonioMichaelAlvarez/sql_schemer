# coding: utf-8
__author__ = 'username'
import sqlite3
from bd_const import *


class SQLGenerator:
    # Класс генератора данных в базе
    def __init__(self, fname, schema_props, domains, tables):
        self.schema_props = schema_props
        self.domains = domains
        self.tables = tables
        print("[SQLGenerator]: Подключение к базе %s" % (fname.replace(".xml", ".sqlite")))
        self.conn = sqlite3.connect(fname.replace(".xml", ".sqlite"))
        self.cur = self.conn.cursor()
        # Список атрибутов для элементов схемы
        self.domain_prop_list = """
        show_null, show_lead_nulls, thousands_separator, summable, case_sensitive
        """
        self.table_prop_list = """
        add, edit, delete
        """
        self.field_prop_list = """
        input, edit, show_in_grid, show_in_details, is_mean, autocalculated, required
        """
        self.constraint_prop_list = """
        has_value_edit, cascading_delete, full_cascading_delete
        """
        # no need in index prop list
        # self.index_prop_list = """
        # """

    def parse_props(self, props_str, props_list):
        # Вспомогательная функция для превращения строки свойств в список
        __pr = props_list.split(",")
        __out_str = []
        for _i in __pr:
            if _i.strip() in props_str:
                __out_str.append("TRUE")
            else:
                __out_str.append("FALSE")
        return __out_str

    def arg_test(self, value):
        # Вспомогательная функция для проверки данных на пустоту
        if "" == value:
            return "NULL"
        else:
            return value

    def prepare_data_domains(self):
        # Функция для подготовки данных доменов
        args = []
        for domain in self.domains:
            tuple_domain = ()
            tuple_domain += self.arg_test(domain.get_prop("name")),
            tuple_domain += self.arg_test(domain.get_prop("description")),
            tuple_domain += self.arg_test(domain.get_prop("type")),
            tuple_domain += self.arg_test(domain.get_prop("length")),
            tuple_domain += self.arg_test(domain.get_prop("char_length")),
            tuple_domain += self.arg_test(domain.get_prop("precision")),
            tuple_domain += self.arg_test(domain.get_prop("scale")),
            tuple_domain += self.arg_test(domain.get_prop("width")),
            tuple_domain += self.arg_test(domain.get_prop("align")),
            for _pr in self.parse_props(domain.get_prop("props"), self.domain_prop_list):
                tuple_domain += _pr,
            tuple_domain += self.arg_test(domain.get_prop("anonymous")),
            args.append(tuple_domain)
        return args

    def prepare_data_tables(self):
        # Функция для подготовки данных таблиц
        args = []
        for table in self.tables:
            tuple_table = ()
            tuple_table += self.arg_test(table.get_prop("name")),
            tuple_table += self.arg_test(table.get_prop("description")),
            for _pr in self.parse_props(table.get_prop("props"), self.table_prop_list):
                tuple_table += _pr,
            args.append(tuple_table)
        return args

    def prepare_data_fields(self):
        # Функция для подготовки данных полей таблиц
        args = []
        for table in self.tables:
            for field in table.fields:
                fld_i = table.fields.index(field)+1
                tuple_field = ()
                tuple_field += table.get_prop("name"),
                tuple_field += fld_i,
                tuple_field += self.arg_test(field.get_prop("name")),
                tuple_field += self.arg_test(field.get_prop("rname")),
                tuple_field += self.arg_test(field.get_prop("description")),
                tuple_field += self.arg_test(field.get_prop("domain")),
                for _pr in self.parse_props(field.get_prop("props"), self.field_prop_list):
                    tuple_field += _pr,
                args.append(tuple_field)
        return args

    def prepare_data_indexes(self):
        # Функция для подготовки данных индексов таблиц
        args = [[], []]
        for table in self.tables:
            for index in table.indexes:
                idx_i = table.indexes.index(index)+1
                idx_n = "idx_%s_%d" % (table.get_prop("name"), idx_i)
                tuple_index, tuple_index_detail = (), ()
                tuple_index += table.get_prop("name"),
                tuple_index += idx_n,
                tuple_index += "TRUE" if "global" in index.get_prop("props") else "FALSE",
                tuple_index += "TRUE" if "uniqueness" in index.get_prop("props") else "FALSE",
                tuple_index += "TRUE" if "fulltext" in index.get_prop("props") else "FALSE",
                args[0].append(tuple_index)
                tuple_index_detail += idx_n,
                tuple_index_detail += idx_i,
                tuple_index_detail += self.arg_test(index.get_prop("field")),
                tuple_index_detail += self.arg_test(index.get_prop("expression")),
                tuple_index_detail += "TRUE" if "descend" in index.get_prop("props") else "FALSE",
                tuple_index_detail += self.arg_test(table.get_prop("name")),
                args[1].append(tuple_index_detail)
        return args


    def prepare_data_constraints(self):
        # Функция для подготовки данных ограничений таблиц
        args = [[], []]
        for table in self.tables:
            parent_tab = table.get_prop("name")
            for constraint in table.constraints:
                cnst_i = table.constraints.index(constraint)+1
                cnst_n = "constr_%s_%d" % (parent_tab, cnst_i)
                tuple_constraint, tuple_constraint_detail = (), ()
                tuple_constraint += parent_tab,
                tuple_constraint += cnst_n,
                tuple_constraint += "P" if "PRIMARY" in constraint.get_prop("kind") else "F",
                tuple_constraint += self.arg_test(constraint.get_prop("reference")),
                for _pr in self.parse_props(constraint.get_prop("props"), self.constraint_prop_list):
                    tuple_constraint += _pr,
                tuple_constraint += self.arg_test(constraint.get_prop("expression")),
                args[0].append(tuple_constraint)
                tuple_constraint_detail += cnst_n,
                tuple_constraint_detail += cnst_i,
                tuple_constraint_detail += self.arg_test(constraint.get_prop("items")),
                tuple_constraint_detail += parent_tab,
                args[1].append(tuple_constraint_detail)
        return args

    def prepare_data_schema(self):
        # Функция для подготовки свойств схемы
        args = []
        for param in self.schema_props.keys():
            tuple_prop = ()
            tuple_prop += param,
            tuple_prop += self.schema_props[param],
            args.append(tuple_prop)
        return args

    def process_target(self, target):
        # Централизованная подготовка данных и их запись в БД
        if "schema_props" == target:
            arg_list = self.prepare_data_schema()
            self.cur.executemany(DML_ADD_SCHEMA_PROPS, arg_list)
        elif "domain" == target:
            arg_list = self.prepare_data_domains()
            self.cur.executemany(DML_ADD_DOMAIN, arg_list)
            return
        elif "table" == target:
            arg_list = self.prepare_data_tables()
            self.cur.executemany(DML_ADD_TABLE, arg_list)
            return
        elif "field" == target:
            arg_list = self.prepare_data_fields()
            # print(arg_list)
            self.cur.executescript(SQL_CREATE_TEMP_FIELDS)
            self.cur.executemany(DML_ADD_FIELD, arg_list)
            self.cur.executescript(DML_JOIN_AND_SAVE_FIELDS)
        elif "index" == target:
            arg_list = self.prepare_data_indexes()
            # print(arg_list)
            self.cur.executescript(SQL_CREATE_TEMP_INDEXES)
            self.cur.executescript(SQL_CREATE_TEMP_INDEXES_DETAILS)
            self.cur.executemany(DML_ADD_INDEX, arg_list[0])
            self.cur.executemany(DML_ADD_INDEX_DETAIL, arg_list[1])
            self.cur.executescript(DML_JOIN_AND_SAVE_INDEXES)
        elif "constraint" == target:
            arg_list = self.prepare_data_constraints()
            self.cur.executescript(SQL_CREATE_TEMP_CONSTRAINTS)
            self.cur.executescript(SQL_CREATE_TEMP_CONSTRAINT_DETAILS)
            self.cur.executemany(DML_ADD_CONSTRAINT, arg_list[0])
            self.cur.executemany(DML_ADD_CONSTRAINT_DETAIL, arg_list[1])
            self.cur.executescript(DML_JOIN_AND_SAVE_CONSTRAINTS)

    def write_sql(self):
        # Запись данных в БД
        print("[SQLGenerator]: Подготовка SQL")
        print("[SQLGenerator]: Инициализация структур таблиц")
        self.cur.executescript(SQL_BD_INIT)
        self.conn.commit()
        print("[SQLGenerator]: Запись данных")
        self.process_target("schema_props")
        self.process_target("domain")
        self.process_target("table")
        self.process_target("field")
        self.process_target("index")
        self.process_target("constraint")
        self.conn.commit()
        self.conn.close()
# coding: utf-8
__author__ = "username"


class Domain:
    def __init__(self):
        self.name = None            # Имя домена
        self.align = None           # Выравнивание
        self.type = None            # Тип
        self.width = None           # Ширина визуализации
        self.char_length = None     # Количество символов
        self.description = None     # Описание
        self.props = None           # Свойства
        self.precision = None       # Точность для вещественных
        self.scale = None           # ?
        self.length = None          # Длина
        self.anonymous = False      # Анонимный домен?

    def set_prop(self, attribute, value):
        self.__dict__[attribute] = value

    def get_prop(self, attribute):
        return self.__dict__[attribute]


class Table:
    def __init__(self):
        self.name = None            # Имя
        self.description = None     # Описание
        self.props = None           # Свойства
        self.fields = []            # Список полей
        self.indexes = []           # Список индексов
        self.constraints = []       # Список ограничений

    def set_prop(self, attribute, value):
        self.__dict__[attribute] = value

    def get_prop(self, attribute):
        return self.__dict__[attribute]

    def find_field(self, name):
        for field in self.fields:
            if field.get_prop("name") == name:
                return field
        return None


class Field:
    def __init__(self):
        self.name = None            # Имя
        self.domain = None          # Домен
        self.data_type = None       # Тип данных
        self.char_length = None     # Количество символов
        self.rname = None           # Имя на русском
        self.props = None           # Свойства
        self.description = None     # Описание

    def set_prop(self, attribute, value):
        self.__dict__[attribute] = value

    def get_prop(self, attribute):
        return self.__dict__[attribute]


class Index:
    def __init__(self):
        self.field = None           # Поле, по которому строить индекс
        self.props = ""             # Свойства
        self.expression = None      # Контрольное выражение
        self.kind = None            # Тип индекса
        self.direction = None       # Направление индекса

    def set_prop(self, attribute, value):
        self.__dict__[attribute] = value

    def get_prop(self, attribute):
        return self.__dict__[attribute]


class Constraint:
    def __init__(self):
        self.items = None           # Поля, к которым применяются ограничения
        self.kind = None            # Тип ограничения (PK или FK)
        self.reference = None       # Таблица, на первичный ключ которой ссылается ограничение
        self.props = None           # Свойства
        self.expression = None      # Контрольное выражение

    def set_prop(self, attribute, value):
        self.__dict__[attribute] = value

    def get_prop(self, attribute):
        return self.__dict__[attribute]
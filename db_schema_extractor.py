# coding: utf-8
__author__ = 'username'
import psycopg2
from bd_const import SQL_GET_DB_DOMAINS, SQL_GET_DB_INDEXES, SQL_GET_DB_TABLES, SQL_GET_DB_CONSTRAINTS
from op_types import Domain, Table, Field, Index, Constraint
from xml_gen import XMLGen
from ddl_builder import DDLBuilder
from datetime import date


class SchemaExtractor:
    """
    Класс, отвечающий за экспортирование метаданных из БД
    """
    def __init__(self, host, user, password, db_source, db_target, mode):
        # Название исходной БД
        self.db_source = db_source
        # Название целевой БД
        self.db_target = db_target
        # Режим работы с данным. Migrate — перенос данных из базы в базу, describe — построение описателя
        self.mode = mode

        self.conn_str_src = "host='%s' dbname='%s' user='%s' password='%s'" % (host, db_source, user, password)
        self.conn_str_tgt = "host='%s' dbname='%s' user='%s' password='%s'" % (host, db_target, user, password)
        self.conn = psycopg2.connect(self.conn_str_src)
        self.cur = None
        if self.conn is None:
            print("[SchemaExtractor]: Ошибка подключения к базе данных")
            exit(1)
        else:
            print("[SchemaExtractor]: Подключен к базе данных")
            self.cur = self.conn.cursor()
        self.tables = []
        self.domains = []

    def find_table_index(self, table_name):
        """
        Функция нахождения индекса таблицы в списке таблиц
        """
        for table in self.tables:
            if table_name == table.name:
                return self.tables.index(table)

    # Переводит номенклатуру udt в понятный для PGSQL вид
    def pg_data_to_universal(self, udt):
        if self.mode == "migrating":
            if udt == "int2":
                return "smallint"
            elif udt == "int8":
                return "bigint"
            else:
                return udt
        else:
            udt = udt.upper()
            if udt == 'BYTEA':
                return 'BLOB'
            elif udt == 'BOOLEAN':
                return 'BOOLEAN'
            elif udt == 'CHAR':
                return 'BYTE'
            elif udt == 'VARCHAR(20)':
                return 'CODE'
            elif udt == 'DATE':
                return 'DATE'
            elif udt == 'BIGINT' or udt == 'INT8':
                return 'LARGEINT'             # int8
            elif udt == 'TEXT':
                return 'MEMO'
            elif udt == 'VARCHAR':
                return 'STRING'
            elif udt == 'TIME':
                return 'TIME'
            elif udt == 'SMALLINT' or udt == 'INT2':
                return 'WORD'           # int2
            else:
                return udt

    def get_domains(self):
        """
        Процедура накопления информации о доменах в БД
        """
        self.cur.execute(SQL_GET_DB_DOMAINS)
        for db_domain in self.cur:
            domain = Domain()
            domain.name = db_domain[0]
            domain.type = self.pg_data_to_universal(db_domain[1])
            domain.char_length = db_domain[2] if db_domain[2] is not None else ""
            self.domains.append(domain)

    def get_tables(self):
        """
        Процедура накопления информации о таблицах в БД
        """
        self.cur.execute(SQL_GET_DB_TABLES)
        cur_table = None
        for db_table in self.cur:

            if cur_table is None:
                cur_table = Table()
                cur_table.name = db_table[0]

            if db_table[0] != cur_table.name:
                self.tables.append(cur_table)
                cur_table = Table()
                cur_table.name = db_table[0]

            if db_table[0] == cur_table.name:
                field = Field()
                field.name = db_table[2]
                field.props = "nullable" if db_table[3] == 'YES' else None
                if db_table[4] is None:
                    domain = Domain()
                    domain.anonymous = True
                    domain.type = self.pg_data_to_universal(db_table[5])
                    domain.char_length = db_table[6] if db_table[6] is not None else ""
                    field.domain = domain
                else:
                    for d in self.domains:
                        if d.name == db_table[4]:
                            field.domain = d
                cur_table.fields.append(field)
        self.tables.append(cur_table)

    def get_indexes(self):
        """
        Процедура накопления информации об индексах в БД
        """
        self.cur.execute(SQL_GET_DB_INDEXES)
        cur_table = ""
        cur_table_index = -1
        idx = None
        for db_index in self.cur:
            if cur_table == "":
                cur_table = db_index[0]
                cur_table_index = self.find_table_index(cur_table)
                idx = Index()

            if cur_table != db_index[0]:
                cur_table = db_index[0]
                cur_table_index = self.find_table_index(cur_table)
                idx = Index()

            if cur_table == db_index[0]:
                idx.field = db_index[1]
                self.tables[cur_table_index].indexes.append(idx)
                idx = Index()



    def get_contstraints(self):
        """
        Процедура накопления информации об ограничениях в БД
        """
        self.cur.execute(SQL_GET_DB_CONSTRAINTS)
        cur_table = ""
        cur_table_index = -1
        constraint = None
        for db_constr in self.cur:
            if cur_table == "":
                cur_table = db_constr[0]
                cur_table_index = self.find_table_index(cur_table)
                constraint = Constraint()

            if cur_table != db_constr[0]:
                cur_table = db_constr[0]
                cur_table_index = self.find_table_index(cur_table)
                constraint = Constraint()

            if cur_table == db_constr[0]:
                constraint.kind = 'PRIMARY' if db_constr[1] == 'PRIMARY KEY' else 'FOREIGN'
                constraint.items = db_constr[2]
                constraint.reference = None if constraint.kind == 'PK' else db_constr[3]
                self.tables[cur_table_index].constraints.append(constraint)
                constraint = Constraint()

    def copy_data(self):
        """
        Процедура переноса информации из одной БД в другую
        """
        # Получить структуру БД
        self.get_domains()
        self.get_tables()
        self.get_indexes()
        self.get_contstraints()
        # Сгенерировать DDL для создания новой базы
        ddlb = DDLBuilder(None, None, self.domains, self.tables)
        ddlb.crouch_data()

        # Список, сожержащий данные из всех таблиц исходной базы
        data = []

        # Получить все данные из всех таблиц, хранящихся в БД
        for table in self.tables:
            self.cur.execute("SELECT * FROM %s;" % table.name)
            data.append(self.cur.fetchall())

        # Чтобы была возможность создавать новые базы, необходимо аутентифицироваться с у/з postgres
        # Данная команда позволяет удалять и добавлять базы
        self.conn.set_isolation_level(0)
        self.cur.execute("DROP DATABASE IF EXISTS \"%s\"" % self.db_target)
        self.cur.execute("CREATE DATABASE \"%s\"" % self.db_target)

        self.conn.close()

        # Открыть соединение с новой базой
        self.conn = psycopg2.connect(self.conn_str_tgt)
        self.cur = None
        if self.conn is None:
            print("[SchemaExtractor]: Ошибка подключения к базе данных")
            exit(1)
        else:
            self.cur = self.conn.cursor()

        # Выполнить DDL для создания структуры БД
        self.cur.execute(ddlb.script)
        self.conn.commit()

        # В каждую таблицу добавить сохранённые ранее данные
        for table in self.tables:
            if len(data[self.tables.index(table)]) > 0:
                # Таблица могла не содержать данных, поэтому её следует пропустить

                # В команде варьируется количество аргументов
                sql = "INSERT INTO \""+table.name+"\" VALUES ("
                for i in range(len(data[self.tables.index(table)][0])-1):
                    sql += "%s,"
                sql += "%s);"
                # Записать данные в таблицу
                self.cur.executemany(sql, data[self.tables.index(table)])

        self.conn.commit()
        self.conn.close()

    def gen_xml(self):
        """
        Процедура накопления метаданных БД и построения XML-описателя
        """
        # Получить структуру БД
        self.get_domains()
        self.get_tables()
        self.get_indexes()
        self.get_contstraints()
        # Получить объект текущей даты
        time_stamp = date.today()
        # Передать генератору XML-описателя название файла и реляционное представление БД
        xmlg = XMLGen(
            self.db_source + "_descriptor_" + str(time_stamp.year) + str(time_stamp.month) + str(time_stamp.day),
            {},
            self.domains,
            self.tables
        )
        # Записать XML-описатель в файл
        xmlg.print_data()

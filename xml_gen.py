# coding: utf-8
__author__ = 'username'
from xml.dom import minidom
from minidom_patch import writexml_nosort, ensure_attributes_with_dict


class XMLGen:
    def __init__(self, file, schema_props, domains, tables):
        print("[XMLGen]: Генерация XML-описателя")
        self.file = file
        self.schema_props = schema_props
        self.domains = domains
        self.tables = tables
        # Список атрибутов для элементов схемы
        self.attr_list_for = dict(
            domain=['name', 'description', 'type', 'align', 'width', 'precision', 'props', 'char_length',
                    'scale', 'length'],
            table=['name', 'description', 'props'],
            field=['name', 'rname', 'domain', 'description', 'props'],
            constraint=['kind', 'items', 'reference', 'expression', 'props'],
            index=['field', 'props', 'expression']
        )

    def xml_gen_doc_root(self):
        # Генерация и заполнение корневого элемента схемы
        doc_root = minidom.Document().createElement("dbd_schema")
        for prop in self.schema_props.keys():
            doc_root.setAttribute(str(prop), str(self.schema_props[prop]))
        return doc_root

    def xml_gen_domains(self):
        # Генерация и заполнение элементов с информацией о доменах
        domains = minidom.Document().createElement("domains")
        for domain_elem in self.domains:
            if not domain_elem.get_prop('anonymous'):
                x_domain = minidom.Document().createElement("domain")
                for attr in self.attr_list_for['domain']:
                    if domain_elem.get_prop(attr) != "" and domain_elem.get_prop(attr) is not None:
                        x_domain.setAttribute(str(attr), str(domain_elem.get_prop(attr)))
                        domains.appendChild(x_domain)
        return domains

    def xml_gen_tables(self):
        # Генерация и заполнение элементов с информацией о таблицах
        tables = minidom.Document().createElement("tables")
        for table_elem in self.tables:
            x_table = minidom.Document().createElement("table")
            for attr in self.attr_list_for['table']:
                if table_elem.get_prop(attr) != "" and table_elem.get_prop(attr) is not None:
                    x_table.setAttribute(str(attr), str(table_elem.get_prop(attr)))
            fields = self.xml_gen_fields(table_elem)
            constraints = self.xml_gen_constraints(table_elem)
            indexes = self.xml_gen_indexes(table_elem)
            for source in [fields, constraints, indexes]:
                for element in source:
                    x_table.appendChild(element)
            tables.appendChild(x_table)
        return tables

    def xml_gen_fields(self, source):
        # Генерация и заполнение элементов с информацией о полях таблицы source
        fields = []
        for field in source.fields:
            x_field = minidom.Document().createElement("field")
            for attr in self.attr_list_for['field']:
                if field.get_prop(attr) != "" and field.get_prop(attr) is not None:
                    if attr != 'domain':
                        x_field.setAttribute(str(attr), str(field.get_prop(attr)))
                    else:
                        if not field.get_prop("domain").get_prop("anonymous"):
                            x_field.setAttribute("domain", str(field.get_prop("domain").get_prop("name")))
                        else:
                            a_domain = field.get_prop("domain")
                            for d_attr in self.attr_list_for['domain']:
                                if d_attr != "name" and a_domain.get_prop(d_attr) != "" and a_domain.get_prop(d_attr) is not None:
                                    x_field.setAttribute("domain."+d_attr, str(a_domain.get_prop(d_attr)))
                    fields.append(x_field)
        return fields

    def xml_gen_constraints(self, source):
        # Генерация и заполнение элементов с информацией об ограничениях таблицы source
        constraints = []
        for constraint in source.constraints:
            x_constraint = minidom.Document().createElement("constraint")
            for attr in self.attr_list_for['constraint']:
                if constraint.get_prop(attr) != "" and constraint.get_prop(attr) is not None:
                    x_constraint.setAttribute(str(attr), str(constraint.get_prop(attr)))
                    constraints.append(x_constraint)
        return constraints

    def xml_gen_indexes(self, source):
        # Генерация и заполнение элементов с информацией об индексах таблицы source
        indexes = []
        for index in source.indexes:
            x_index = minidom.Document().createElement("index")
            for attr in self.attr_list_for['index']:
                if index.get_prop(attr) != "" and index.get_prop(attr) is not None:
                    x_index.setAttribute(str(attr), str(index.get_prop(attr)))
                    indexes.append(x_index)
        return indexes

    def print_data(self):
        # Запись данных в файл
        minidom.Element._ensure_attributes = ensure_attributes_with_dict
        minidom.Element.writexml = writexml_nosort
        doc = minidom.Document()
        doc_root = self.xml_gen_doc_root()
        domains = self.xml_gen_domains()
        tables = self.xml_gen_tables()
        doc_root.appendChild(domains)
        doc_root.appendChild(tables)
        doc.appendChild(doc_root)
        with open(self.file + "_recovered.xml", "w") as f:
            f.write(str(doc.toprettyxml(indent="  ")))